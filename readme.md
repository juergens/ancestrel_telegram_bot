# Ancestral Telegram Bot

This is a simple telegram-bot, that searches https://darkestdungeon.fandom.com/wiki/Narrator_(Darkest_Dungeon) for quotes from the game "
Darkest Dungeon" and then links to the voicelines of these quotes

For details on how to use it, ask the bot directly (with the `help` command).

## deployment

This section is only relevant if you are me

    cd /srv
    git clone git@gitlab.com:juergens/ancestrel_telegram_bot.git
    cd ancestrel_telegram_bot
    
    nano secret.py # define variable "telegram_token"
    
    docker build -t ancestrel_bot .  
    docker run --rm -d --name ancestrel_bot ancestrel_bot

## update

    cd /srv/ancestrel_telegram_bot && git pull --rebase && docker build -t ancestrel_bot . && docker kill ancestrel_bot; docker rm ancestrel_bot; docker run -d --rm --name ancestrel_bot ancestrel_bot && docker logs -ft ancestrel_bot
