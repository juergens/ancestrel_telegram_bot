FROM python:3.8-slim

WORKDIR /bot
COPY requirements.txt /bot
RUN pip3 install -r requirements.txt

COPY main.py /bot/main.py
COPY secret.py /bot/secret.py
COPY src /bot/src
ENV PYTHONUNBUFFERED 0
CMD ["python3", "main.py"]
