import logging
from typing import List

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from telegram.ext import Updater

import secret
from src.models import Base, DBFacade, Quote
from src.telegram_handlers import AncestralInlineQueryHandler, HelpHandler, error_handler
from src.voice_fetcher import VoiceFetcher

logging.basicConfig(format='%(asctime)s-%(name)s-%(levelname)s: %(message)s', level=logging.INFO)
logger = logging.getLogger(__name__)

engine = create_engine('sqlite:///test.sqlite')
Base.metadata.create_all(engine)
db_session_factory = sessionmaker(bind=engine)

db: DBFacade = DBFacade(db_session_factory)

if not db.has_quotes():
    logging.warning("No Quotes found in DB. Downloading quotes now (may take a couple of minutes)")
    quotes: List[Quote] = VoiceFetcher().fetch()
    db.save(*quotes)

updater = Updater(secret.telegram_token)
updater.dispatcher.add_error_handler(error_handler)

updater.dispatcher.add_handler(AncestralInlineQueryHandler(db))
updater.dispatcher.add_handler(HelpHandler("help"))
updater.dispatcher.add_handler(HelpHandler("start"))

updater.start_polling()
updater.idle()

# for q in db.search_quotes(""):
#    print(q.audio_url)
