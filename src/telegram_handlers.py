import logging
from typing import Union

from telegram import InlineQueryResultAudio, ParseMode, Update
from telegram.ext import CallbackContext, CommandHandler, InlineQueryHandler

from src.models import DBFacade
from src.voice_fetcher import base_url


def error_handler(update: Union[object, Update], context: CallbackContext) -> None:
    logging.error(msg="Exception while handling an update:", exc_info=context.error)
    if update and update.effective_message:
        update.effective_message.reply_text(
            "OOPSIE WOOPSIE!! Uwu We made a fucky wucky!! A wittle fucko boingo! "
            "The code monkeys at our headquarters are working VEWY HAWD to fix this!")


def _shorten(s: str, n: int) -> str:
    if len(s) <= n:
        return s
    return f"{s[:n - 3]}..."


class AncestralInlineQueryHandler(InlineQueryHandler):

    def __init__(self, db: DBFacade):
        super(AncestralInlineQueryHandler, self).__init__(self._inline_query_callback)
        self.db: DBFacade = db

    def _inline_query_callback(self, update: Update, _: CallbackContext) -> None:
        """Handle the inline query."""
        query = update.inline_query.query

        if query == "":
            return

        relevant_quotes = self.db.search_quotes(query)
        relevant_quotes_by_section = self.db.search_quotes_section(query)
        results = []

        if len(relevant_quotes) < 1 and len(relevant_quotes_by_section) < 1:
            return

        for q in relevant_quotes_by_section:
            logging.debug(q)
            r = InlineQueryResultAudio(
                id=str(f"sect_{q.id}"),
                title=f"{_shorten(q.section_name, 15)}: {_shorten(q.text, 50)}",
                audio_url=q.audio_url,
                caption=f"{q.text}",
                performer="Wayne June"
            )
            results.append(r)

        for q in relevant_quotes:
            logging.debug(q)
            r = InlineQueryResultAudio(
                id=str(f"text_{q.id}"),
                title=f"{_shorten(q.text, 50)}",
                audio_url=q.audio_url,
                caption=f"{q.text}",
                performer="Wayne June"
            )
            results.append(r)

        update.inline_query.answer(results[:50])


class HelpHandler(CommandHandler):

    def __init__(self, command: str):
        super(HelpHandler, self).__init__(command, self.callback)

    def callback(self, update: Update, _: CallbackContext) -> None:
        # https://darkestdungeon.fandom.com/wiki/Narrator
        update.effective_message.reply_text(
            f"This bot searches the darkest-dungeon wiki for narrator quotes and outputs them as voice messages. \n\n"
            f"To use this bot, simply type `@ancestrel_bot ` into telegram's text-box followed by a search term. \n"
            f"The bot will then search its database for quotes containing the search term and you can select the "
            f"quote you to send to the current chat. "
            f"\n\n"
            f"The voice lines are are located at {base_url}\n\n"
            f"The bot's source code licensed under AGPL and is located "
            f"at https://gitlab.com/juergens/ancestrel\_telegram\_bot/\n\n"
            f"credits go to: \n"
            f"- the contributors of the darkest-dungeon wiki for maintaining the website, where the "
            f"bot gets its quotes from,\n"
            f"- _redhookgames_ for creating the game _Darkest Dungeon_ and\n"
            f"- _Wayne June_ for narrating the voice lines in the first place\n\n",
            parse_mode=ParseMode.MARKDOWN)
        pass
