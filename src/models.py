from __future__ import annotations  # because PEP563 Postponed Evaluation of Annotations

from typing import List

from sqlalchemy import Column, Integer, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm.session import sessionmaker

Base = declarative_base()


class TelegramChat(Base):
    __tablename__ = 'telegram_chat'
    id = Column(Integer, primary_key=True, autoincrement=True)
    telegram_chat_id: int = Column(Integer, unique=True, nullable=False)


class Quote(Base):
    __tablename__ = 'ancestral_quote'
    id = Column(Integer, primary_key=True, autoincrement=True)
    text: str = Column(String, nullable=False)
    section_name = Column(String, nullable=True)
    audio_url: str = Column(String, nullable=False)

    # audio_duration: int = Column(Integer, nullable=False)

    def __repr__(self):
        return "<Quote(id='%s', text='%s', section_name='%s'>" % (
            self.id, self.text, self.section_name)


class DBFacade:
    """Facade that hides database-access from actual code.
    helps to realize DRY and SLA"""

    def __init__(self, db_session_factory: sessionmaker):
        self.sm: sessionmaker = db_session_factory

    def save(self, *nargs):
        s = self.sm()
        s.add_all(nargs)
        s.commit()
        s.close()

    def has_quotes(self):
        s = self.sm()
        ret = s.query(Quote).first() is not None
        s.close()
        return ret

    def search_quotes(self, text: str) -> List[Quote]:
        s = self.sm()
        ret = s.query(Quote).filter(Quote.text.ilike(f"%{text}%")).all()
        s.close()
        return ret

    def search_quotes_section(self, text: str) -> List[Quote]:
        s = self.sm()
        ret = s.query(Quote).filter(Quote.section_name.ilike(f"%{text}%")).all()
        s.close()
        return ret
