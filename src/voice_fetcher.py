import logging
from typing import Optional

import requests
from bs4 import BeautifulSoup

from src.models import Quote

base_url: str = "https://darkestdungeon.fandom.com/wiki/Narrator_(Darkest_Dungeon)"


class VoiceFetcher:

    def __init__(self):
        pass

    def _get_section_name(self, link) -> Optional[str]:
        element = link.parent.parent
        while element is not None:
            if element.name == "h3" or element.name == "p":
                return next(element.children).text
            element = element.previousSibling
        return None

    def fetch(self):
        req = requests.get(base_url)
        soup = BeautifulSoup(req.content, 'html.parser')

        final_link = soup.p.a
        final_link.decompose()

        links = soup.select('audio a')
        quotes = []
        logging.info(f"found {len(links)} quotes at {base_url}")

        for link in links[:]:
            try:
                section = self._get_section_name(link)
                text = link.parent.parent.previousSibling.previousSibling.previousSibling.text.strip("“„\n")
                audio_href = link.get("href")
                q = Quote(text=text, section_name=section, audio_url=audio_href)
                quotes.append(q)
                logging.info(q)
                logging.info(f"downloaded {len(quotes)} out of {len(links)}")
            except Exception as e:
                logging.error(f"encountered exception when processing {link}.")

        return quotes
